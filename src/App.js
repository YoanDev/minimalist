import React from 'react'
import Home from './pages/home'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import About from './pages/about'
import Artistic from './pages/artistic'
import Contact from './pages/contact'

const App = () => {
  let routes
  routes = (
    <>

    <Route path="/" component={Home} exact/>
    <Route path="/about" component={About} exact/>
    <Route path="/artistic" component={Artistic} exact/>
    <Route path="/contact" component={Contact} exact/>


    </>
  )


  return (
    <>
        
        <Router>

          <Switch>
            {routes}
          </Switch>

        </Router>
    </>
  )
}

export default App
