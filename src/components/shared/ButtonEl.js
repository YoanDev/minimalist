import React from 'react'
import './button.css'

const ButtonEl = props => {
    return (
        <>
            <button className={props.classNames} onClick={props.onClick}>
                {props.children}
            </button>
        </>
    )
}

export default ButtonEl
