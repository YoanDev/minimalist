import React, {useEffect, useState} from 'react'
import {NavLink} from 'react-router-dom'
import './navbar.css'
import Logo from '../../../../img/apple.svg'
import {FiMenu} from 'react-icons/fi'

//Aos
import 'aos/dist/aos.css'
import Aos from 'aos'
import Sidebar from '../Sidebar/sidebar'
import ButtonEl from '../../../shared/ButtonEl'

const Navbar = props => {
    const [toggle, setToggle] = useState(false)

    const mobileHandler = ()=>{
        setToggle(!toggle)
        console.log(toggle)
    }
    useEffect(()=>{
        Aos.init({duration:2000})

    },[])
    return (
        <>
            <nav data-aos="fade-out" className="nav-container">
                <ul className="navbar">
                    <NavLink to="/"><img className="logo-nav" src={Logo} alt="logo"/></NavLink>
                    <NavLink to="/"><li className="navlinks">Home</li></NavLink>
                    <NavLink to="/about"><li className="navlinks">About</li></NavLink>
                    <NavLink to="/artistic"><li className="navlinks">Artistic</li></NavLink>
                    <NavLink to="/contact"><li className="navlinks">Contact us</li></NavLink>
                    <ButtonEl classNames="button-nav" onClick={()=> console.log('click')}>Join us</ButtonEl>
                    <FiMenu className="mobile-bar" onClick={mobileHandler} />
                </ul>
            </nav>
            {toggle && (
                <Sidebar aos="slide-left" onClickArrow={mobileHandler} />
            )}
        </>
    )
}

export default Navbar
