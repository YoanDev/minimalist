import React, {useEffect} from 'react'
import BgHero from '../../../../img/img1.svg'
import './hero.css'

//Aos
import 'aos/dist/aos.css'
import Aos from 'aos'

const Hero = props => {
    useEffect(()=>{
        Aos.init({duration:2000})

    },[])

    return (
        <>
            <section data-aos={props.aos} className="container-hero">
                <div className="box-hero">
                        <div className="picture">
                            <img src={props.src} alt={props.alt}/>
                        </div>
                    <div data-aos="slide-left" className="content-hero">
                        <h2> {props.h2} </h2>
                        <p> {props.p} </p>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Hero
