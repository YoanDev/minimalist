import React, {useEffect} from 'react'
import './sidebar.css'
import {NavLink} from 'react-router-dom'
import {BsArrowLeftShort} from 'react-icons/bs'

//Aos
import 'aos/dist/aos.css'
import Aos from 'aos'
import ButtonEl from '../../../shared/ButtonEl'

const Sidebar = props => {
    useEffect(()=>{
        Aos.init({duration:2000})

    },[])
    return (
        <>
            <aside data-aos={props.aos} className="sidebar-container">
                <ul className="sidebar">
                        <BsArrowLeftShort className="mob-arrow" onClick={props.onClickArrow} />
                        <NavLink to="/"><li className="moblinks">Home</li></NavLink>
                        <NavLink to="/about"><li className="moblinks">About</li></NavLink>
                        <NavLink to="/artistic"><li className="moblinks">Artistic</li></NavLink>
                        <NavLink to="/contact"><li className="moblinks">Contact us</li></NavLink>
                        <ButtonEl classNames="button-side" onClick={()=> console.log('click')}>Join us</ButtonEl>
                    </ul>
            </aside>
        </>
    )
}

export default Sidebar
