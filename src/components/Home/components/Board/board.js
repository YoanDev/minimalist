import React, { useState, useEffect } from 'react'
import './board.css'
import Items from './Items'
import axios from 'axios'

const Board = () => {
    const [responseData, setResponseData] = useState([])
    const [refreshTable, setRefreshTable] = useState(true)


    useEffect(() => {
        const getData = () => {
            axios.get('https://jsonplaceholder.typicode.com/todos/')
                .then((data) => {
                    console.log("data", data)
                    if (data.status >= 200 && data.status < 300) {
                        setResponseData(data.data)
                    }
                }).catch((err) => {
                    console.log("err", err)
                })
            
        }

        if (refreshTable) {
            getData();
            setRefreshTable(false)
        }
        // setResponseData(responsesData)
        // console.log(responsesData)
    }, [responseData, refreshTable])



    return (
        <>
            <Items items={responseData} />
        </>
    )
}

export default Board
