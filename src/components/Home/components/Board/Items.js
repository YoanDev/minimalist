import React, { useState, useEffect } from 'react';
import './items.css';

const Items = props => {
    const [dataToShow, setDataToShow] = useState([]);
    useEffect(() => {
        
        const temp = props.items.slice(0,50).map((value, index) =>
            <>
                    <div className="data-box" key={index}>
                        <div className="data-content">
                            <p>User: {value.id}</p>
                            <h2>{value.title}</h2>
                        </div>
                    </div>
            </>
        )
        setDataToShow(temp)
    }, [props])
    if(props.items.length === 0){
        console.log("ca passe dedans")
        return (
            <div className="warning">
            <div className="content">
            <div className="loader">Loading...</div>
            </div>
        </div>
        )
    }
    return (
        <>
        <div className="data-container">

            {dataToShow}
        </div>
        </>
    )
}

export default Items
