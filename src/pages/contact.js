import React from 'react'
import Hero from '../components/Home/components/Hero/hero'
import Navbar from '../components/Home/components/Navbar/navbar'
import Bg4 from '../img/img4.svg'

const Contact = () => {
    return (
        <>
            <Navbar />
            <Hero 
            src={Bg4}
            alt={`contact-img`}
            h2={`Contact`}
            p={`You can contact me via instagram`}
             aos={`fade-out`}
             />
        </>
    )
}

export default Contact
