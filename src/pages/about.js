import React from 'react'
import Hero from '../components/Home/components/Hero/hero'
import Navbar from '../components/Home/components/Navbar/navbar'
import Bg2 from '../img/img2.svg'


const About = () => {
    return (
        <>
            <Navbar />
            <Hero
             src={Bg2}
             alt={`about-img`}
             h2={`About`}
             p={`Website based on react js, website created by web4design`}
             aos={`fade-out`}
             />
        </>
    )
}

export default About
