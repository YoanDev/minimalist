import React from 'react'
import Board from '../components/Home/components/Board/board'
import Hero from '../components/Home/components/Hero/hero'
import Navbar from '../components/Home/components/Navbar/navbar'
import Bg1 from '../img/img1.svg'

const Home = () => {
    return (
        <>
            <Navbar />
            <Hero
             src={Bg1}
             alt={`home-img`}
             h2={`Home`}
             p={`Minimalist design is the most popular way and secure way to design your websites`} 
             aos={`fade-out`}
             />

             <Board />
        </>
    )
}

export default Home
