import React from 'react'
import Hero from '../components/Home/components/Hero/hero'
import Navbar from '../components/Home/components/Navbar/navbar'
import Bg3 from '../img/img3.svg'


const Artistic = () => {
    return (
        <>
            <Navbar />
            <Hero
             src={Bg3}
             alt={`img-artistic`}
             p={`Let place to your creativity`}
             h2={`Artistic`}
             aos={`fade-out`}
             />
        </>
    )
}

export default Artistic
